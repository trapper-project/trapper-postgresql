#!/bin/bash

docker buildx build  \
      --progress=plain \
      -t "$CI_REGISTRY_IMAGE:$IMAGE_TAG" \
      --platform linux/amd64,linux/arm64 \
      .