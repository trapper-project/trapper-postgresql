FROM postgres:16

RUN apt-get update && \
    apt-get install -y --no-install-recommends postgresql-16-postgis-3 && \
    apt-get clean

COPY hstore.sql /docker-entrypoint-initdb.d
